# Calculator GAIA

Repository for DEV Talks

Installation

```
composer install
```

Run tests

```
vendor/bin/phpunit
```

OR

```
vendor/bin/phpunit --filter=CalculatorTest
```

Thanks,
Jonathan
