<?php

namespace Gaia;

use InvalidArgumentException;

class Calculator
{
    public function sum($a, $b)
    {
        if (! is_numeric($a) || ! is_numeric($b)) {
            throw new InvalidArgumentException('Number invalid');
        }

        return $a + $b;
    }
}
