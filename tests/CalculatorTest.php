<?php

namespace Gaia\Tests;

use Gaia\Calculator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /** @test */
    public function it_should_return_the_sum_when_parameters_are_correct()
    {
        $sum = (new Calculator)->sum(2, 2);

        $this->assertEquals(4, $sum);
    }

    /** @test */
    public function it_returns_the_sum_of_the_arguments_even_if_are_string()
    {
        $sum = (new Calculator)->sum('2', '3');

        $this->assertEquals(5, $sum);
    }

    /** @test */
    public function it_should_return_an_exception_when_a_or_b_are_missing()
    {
        try {
            (new Calculator)->sum('one', 2);
        } catch (InvalidArgumentException $exception) {
            $this->assertSame('Number invalid', $exception->getMessage());
        }
    }

    public function test_true_is_true()
    {
        $this->assertTrue(true);
    }
}
